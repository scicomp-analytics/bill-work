#!/usr/bin/env python

"""
Script to process incoming jobs through parrot run for packaging
either for job preservation or to build software pkgs

For now many things are kludgy/messy, and this is very much a work in
progress.
"""

import classad
import os
import sys
import tarfile
from contextlib import closing
from subprocess import call

# TODO: refactor as many calls as possible to native python bindings
# TODO: fix home and other user stuff for parrot

PARROT_PKG_DIR = "/parrotpkg"
# Set this to whatever directory should house all the software packages
# Each package will be stored in BUILD_PKG_DIR/[package_name]
BUILD_PKG_DIR = "/tmp/build/"
# TODO: HOME_DIR can probably be removed. Test if servers come back up..
HOME_DIR = "/tmp"

# Variables to ignore when packaging software as they are job
# Specific. Compiled by trial and error and probably not exhaustive

IGNORE_VAR_LIST = ('_CONDOR_JOB_PIDS', 'HOSTNAME', '_CONDOR_SCRATCH_DIR',
                   '_CHIRP_DELAYED_UPDATE_PREFIX', 'BATCH_SYSTEM',
                   '_CONDOR_ANCESTOR_', '_CONDOR_MACHINE_AD',
                   '_CONDOR_WRAPPER_ERROR_FILE',
                   '_CONDOR_SLOT', 'OMP_NUM_THREADS', '_CONDOR_JOB_AD',
                   '_CONDOR_CHIRP_CONFIG', 'HTTP_PROXY', 'LOGNAME',
                   'COBBLER_SERVER', 'XrdSecGSISRVNAMES', '_CONDOR_JOB_IWD',
                   'PWD', 'USER', 'HOME', 'TMP', 'TEMP', 'TMPDIR')

class ParrotJob(object):
    '''Instance of a job to be preserved by parrot
    pkgTool="Preserve" for jobs to instantly be packed and shipped,
    pkgTool="Build" for jobs which are intended to build a reproduction
    of a software package. Software requires additional
    pkgID="someprogram"
    '''

    def __init__(self):
        self.job_ad = classad.parseOne(open(".job.ad", "r"))

        # pkg_id is a unique ID for jobs to be preserved, or name of
        # package (e.g. 'IceCube' if packaging up software)
        # pkg_dir is execution directory for preservation, or
        # package directory for software packaging

        if self.job_ad['pkgTool'] == 'Preserve':
            job_id = (str(self.job_ad["ClusterId"]) + "." +
                      str(self.job_ad["ProcId"]) + "." +
                      str(self.job_ad["QDate"]))
            self.package_id = self.job_ad["User"] + "." + job_id
            self.pkg_dir = os.getcwd() + PARROT_PKG_DIR
            self.job_type = 'Preserve'

        elif self.job_ad['pkgTool'] == 'Build':
            if ('pkgID' not in self.job_ad) or (self.job_ad['pkgID'] == ''):
                sys.stderr.write("You must specify a package to build!\n")
                sys.exit(1)
            else:
                self.package_id = self.job_ad['pkgID']
                self.pkg_dir = BUILD_PKG_DIR + self.package_id
                self.job_type = 'Build'

        else:
            sys.stderr.write("Invalid pkgTool option. 'Preserve' or 'Build'\n")
            sys.exit(1)

    def is_preserve_job(self):
        '''Returns boolean/true if a preservation job, false otherwise'''
        return True if self.job_type == 'Preserve' else False

    def run(self, args=sys.argv[2:]):
        '''Create/update the parrot package'''

        #TODO: pipe output to both log and regular stderr/out
        namelist_id = 'namelist' + str(self.job_ad["QDate"])
        envlist_id = 'envlist' + str(self.job_ad["QDate"])
        # Setup environment for parrot. May be different for different sites.
        os.environ["PARROT_ALLOW_SWITCHING_CVMFS_REPOSITORIES"] = "yes"
        os.environ["HOME"] = HOME_DIR
        os.environ["HTTP_PROXY"] = "http://uct2-squid.mwt2.org:3128"
        os.environ["LD_LIBRARY_PATH"] = (os.getenv("LD_LIBRARY_PATH", "") +
                                         ":/usr/local/cctools/lib")
        os.environ["PATH"] = (os.getenv("PATH", "") + ":/usr/local/sbin:" +
                              "/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:" +
                              "/usr/local/cctools/bin")

        # Creae environment list minus all the variables we don't want
        with open(envlist_id+".mod","wb") as envfile:
            for var in os.environ.keys():
                if not(var.startswith("_CONDOR_ANCESTOR") or var in IGNORE_VAR_LIST):
                    envfile.write(b"{0}={1}\x00".format(var, os.environ[var]))

        # Execute initial parrot_run (run/trace the program)
        parrot_retval = call("parrot_run --name-list " + namelist_id +
                             " --env-list " + envlist_id + " " + os.getcwd() +
                             "/condor_exec.exe " + " ".join(args), shell=True)
        if parrot_retval != 0:
            sys.stderr.write("parrot_run failed\n")
            sys.exit(1)

        # Create the package from trace
        # Check to see if the directory already exists--if it does,
        # This means we're adding to an existing pkg
        log = open("/tmp/clonejobs.log", "a")
        if os.path.isdir(self.pkg_dir):
            # Add to an existing pkg if it already exists
            parrot_retval = call("parrot_package_create --name-list " +
                                 namelist_id + " --env-list " + envlist_id +
                                 " --new-env " + envlist_id +
                                 " --add " + self.pkg_dir,
                                 stderr=log, shell=True)
        else:
            parrot_retval = call("parrot_package_create --name-list " +
                                 namelist_id + " --env-list " + envlist_id +
                                 " --package-path " + self.pkg_dir,
                                 stderr=log, shell=True)
        if parrot_retval != 0:
            sys.stderr.write("parrot_package_create failed\n")
            log.close()
            sys.exit(1)

        # Move the dumped final environment
        os.rename(envlist_id+".mod", self.pkg_dir+"/"+envlist_id+".mod")

        log.close()


def pack_tar(directory, pkg_name, preserve=True):
    ''' Pack a package into tgz with parrot pkg and tar with metadata 
    directory should be full path of parrot pkg dir
    '''
    os.chdir(directory)
    os.chdir("..")     # Directory given is parrotpkg dir

    # TODO: delete quick hack fixing "parrotpkg"
    # Closing is needed for python 2.6x compatibility (ugh)

    if os.path.isdir(directory):
        with closing(tarfile.open(pkg_name+".tgz", "w:gz", compresslevel=4)) as tar:
            tar.add(directory, arcname="parrotpkg")
            # tar.add("namelist")
            # tar.add("envlist")
    else:
        sys.stderr.write("Directory {0} does not exist\n".format(directory))

    # Only add metadata for preservation jobs (for now)
    if preserve:
        os.rename(".job.ad", "job.ad")
        os.rename(".machine.ad", "machine.ad")

        with closing(tarfile.open(pkg_name+".meta.tar", "w:")) as tar:
            for entry in ["job.ad", "machine.ad", "env.dump", "namelist", "envlist"]:
                tar.add(entry)


def upload_s3(pkg_name, preserve=True):
    ''' Process upload of package to S3'''
    # TODO: rewrite s3cmd call using boto (or something else)?
    # Allow for configuring bucket, etc
    # Also currently assumes same dir as pack_tar
    # no metadata for software pkgs (for now). Only add if preserve job.
    call(["s3cmd", "put", pkg_name+".tgz", "s3://job-packages"])
    if preserve:
        call(["s3cmd", "put", pkg_name+".meta.tar", "s3://job-packages"])


def check_pkg_exists(directory):
    ''' Check if a software pkg exists in given directory'''
    # TODO: directory should really just be name of the pkg to play nice
    # when I add configuring the root pkg path
    return True if os.path.isdir(directory) else False


def main():
    ''' Main function for direct loading'''
    import timeit
    start_time = timeit.default_timer()
    job = ParrotJob()
    job.run()
    job_time = timeit.default_timer() - start_time
    if job.is_preserve_job():
        pack_tar(job.pkg_dir, job.package_id, preserve=True)
        pack_time = timeit.default_timer() - start_time - job_time
        upload_s3(job.package_id, preserve=True)
        upload_time = timeit.default_timer() - start_time - job_time - pack_time
        print("Job time: {0}, tar pack time: {1}, s3 upload time: {2}".format(
              job_time, pack_time, upload_time))
    else: print("Job time: {0}".format(job_time))

if __name__ == "__main__":
    main()
