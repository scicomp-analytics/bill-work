#!/usr/bin/env python
"""
Script to process jobs to be executed on a machine by using parrot packages
created elsewhere. A work in progress, ideally much that is hard-coded here
will eventually be more flexible
"""

import classad
import os
import sys
from subprocess import call

HOME_DIR = "/tmp"
PARROT_USER = "nobody"
PKG_HOST_DIR = "/tmp/build/"
MOUNTLIST_NAME = "mountlist"

def main(args):
    os.environ["HOME"] = HOME_DIR
    os.environ["USER"] = PARROT_USER
    # Parrot variables
    os.environ["PARROT_ALLOW_SWITCHING_CVMFS_REPOSITORIES"] = "yes"
    os.environ["LD_LIBRARY_PATH"] = (os.getenv("LD_LIBRARY_PATH", "") +
                                     ":/usr/local/cctools/lib")
    os.environ["PATH"] = (os.getenv("PATH", "") + ":/usr/local/sbin:" +
                          "/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:" +
                          "/usr/local/cctools/bin")

    job_ad = classad.parseOne(open(".job.ad", "r"))
    pkg_dir = PKG_HOST_DIR + job_ad['parrotRun']
    parrot_str =  "parrot_package_run -p " + pkg_dir + " " + os.getcwd() + "/condor_exec.exe " + " ".join(args)

    if os.path.isdir(pkg_dir):
        retval = call(parrot_str, shell=True)
    else:
        sys.stderr.write("Invalid package specified")
        sys.exit(1)

    if retval != 0:
        sys.stderr.write("An error with parrot_pacakge run_occured")
    else:
        os.remove(MOUNTLIST_NAME)  # Don't want condor to transfer the mountlist on exit


if __name__ == "__main__":
    main(sys.argv[2:])
