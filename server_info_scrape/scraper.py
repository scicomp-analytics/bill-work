#!/usr/bin/env python
import sys
import re
import os

FILEDIR = sys.argv[1] if len(sys.argv[1]) > 1 else "./"
USERNAME_REGEX = re.compile(r'uid=[0-9]+\((.*?)\)')
GROUP_REGEX = re.compile(r'gid=[0-9]+\((.*?)\)')
ENVLIST = ['USER', 'HOSTNAME', '_CONDOR_SCRATCH_DIR', 'TMP',
           'TEMP', 'HOME', 'PWD']
USER_KEY = 'USER'
HOME_KEY = 'HOME'
DIRLIST = ['_CONDOR_SCRATCH_DIR', 'TMP', 'TEMP', 'HOME', 'PWD']
EXCLUDELIST = ['USER']

def build_envars(envline):
    '''Build environment dict from single line where vars are separated
    by nullj'''
    lines = envline.split('\x00')
    envdict = {}
    for line in lines:
        linesplit = line.split("=", 1)
        if len(linesplit) == 2:
            envdict[linesplit[0]] = linesplit[1]
        elif len(linesplit) == 1:
            envdict[linesplit[0]] = None
    return envdict

def check_users(dict_list):
    '''Check if all users for a given list of entries are the same
    Returns a boolean and list of unique user names'''
    # could add check for empty list here
    first_user = dict_list[0][USER_KEY]
    userlist = set([first_user])
    for dictionary in dict_list:
        if dictionary[USER_KEY] not in userlist:
            userlist.add(dictionary[USER_KEY])
    userlist = list(userlist)
    return (True if len(userlist) == 1 else False, userlist)

def check_dirs(dict_list):
    '''Check if all the dirs defined in dirlist are the same. Or if all
    are the same except HOME. Returns 1 if all same, 2 if all but
    home are same, -1 otherwise'''
    all_same = True
    for dictionary in dict_list:
        # PWD is the one that (...should) exist everywhere
        first = dictionary['PWD']
        for entry in DIRLIST[0:-1]:
            if dictionary[entry] != first:
                if entry == 'HOME':
                    all_same = False
                elif dictionary[entry] == None:
                    continue
                else: return -1
    return 1 if all_same else 2

# Dictionary; key = hostname, val = list of dicts for each entry of a host
# Entry values:
# USER, USER.ENV, GROUP,  HOSTNAME.ENV, HOME,
# _CONDOR_SCRATCH_DIR, TMP, TEMP, PWD
# xxx.ENV is if info is also available from environment
def create_info_dict(filepath):
    return_dict = {}
    '''Create a dictionary holding all the info scraped (held in memory,
    so probably shouldn't go above ~100k records). Key = hostname,
    val = list of dicts for each entry of a host, each of which holds
    environment and other variables described above'''
    jobfiles = os.listdir(filepath)
    for jobfile in jobfiles:
        if jobfile == os.path.basename(sys.argv[0]):
            continue # script
        with open(FILEDIR+jobfile, 'r') as logfile:
            content = logfile.readlines()
        # cwd = content[3]
        hostname = content[1].rstrip('\n')
        if hostname not in return_dict:
            return_dict[hostname] = []
        entry = {}
        try:
            entry['USER'] = USERNAME_REGEX.findall(content[2])[0]
            entry['GROUP'] = GROUP_REGEX.findall(content[2])[0]
        except IndexError:
            entry['USER'] = None
            entry['GROUP'] = None
        envs = build_envars(content[5])
        for item in ENVLIST:
            if item in envs.keys():
                if item in DIRLIST:
                    entry[item] = envs[item]
                else:
                    entry[",".join([item, ".ENV"])] = envs[item]
            else:
                if item not in EXCLUDELIST:
                    entry[item] = None
        return_dict[hostname].append(entry)

    return return_dict

def main():
    host_info = create_info_dict(FILEDIR)
    for entry in host_info.keys():
        print("-------------")
        (is_same, user_list) = check_users(host_info[entry])
        extra_host = host_info[entry][0]['HOSTNAME'] if \
            'HOSTNAME' in host_info[entry][0].keys() else None
        print("Host: {0} Hostname (if avail): {1}".format(entry, extra_host))
        print("Occurences: {0}".format(len(host_info[entry])))
        print("All same: {0}, user list: {1}".format(is_same, user_list))
        dir_retval = check_dirs(host_info[entry])
        if dir_retval == 1:
            print("all dirs same: {0}".format(host_info[entry][0]['PWD']))
        elif dir_retval == 2:
            print("all but home same. PWD: {0}, HOME: {1}".format(
                host_info[entry][0]['PWD'], host_info[entry][0]['HOME']))
        else: print("has multiple dir conflicts")

if __name__ == '__main__':
    main()
