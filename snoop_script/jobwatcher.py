#!/usr/bin/env python
'''Script which uses the python watchdog module to monitor number of files and
total directory size for condor jobs. It logs the number of files and size of
files in a job directory over time.

For each job, output is a CSV of format:
header row: User@domain, Date
all following rows: Epoch time, Number files, Directory size (bytes)
Each file is saved as user.domain.date.jobID.procID.log

Requires classad bindings for python and watchdog package
(http://pythonhosted.org/watchdog/installation.html, sometimes available as
rpm/deb)

CONDOR_WATCHDIR may need to be changed if different than
/var/lib/condor/execute

It also will not catch jobs which are very short (<1-2 seconds) due to sleep'''

'''This implementation isn't optimal by any means, but gets the job done. A
master condorwatcher watches the condor directory for new directories (incoming
jobs), and spawns a new process to monitor each job (jobwatcher)'''

# NOTE: recommended taskset to execute, as python2 doesn't have native
# cpu affinity bindings in the os module

# TODO: better implemented as a daemon or something that can interface with
# init/sysctl
# TODO: packaging the logs and upload automatically (or a cron?)

import sys
import classad
import time
import os
from watchdog.observers import Observer
import watchdog
import datetime
import csv
from multiprocessing import Process, Event

LOG_DIR = "/tmp/joblogs/"
CONDOR_WATCHDIR = "/var/lib/condor/execute"

class CondorWatcher(watchdog.events.FileSystemEventHandler):
    '''Master class for watching condor jobs. Creates a new JobWatcher method
    each time another job is spawned'''

    def __init__(self, watchpath=CONDOR_WATCHDIR):
        observer = Observer()
        observer.schedule(self, watchpath, recursive=False)
        observer.start()
        self.job_watchers = {}
        while True:
            time.sleep(1)

    def on_created(self, event):
        '''Override FSH method. If new dir, create a JobWatcher for it'''
        if event.is_directory:
            self.job_watchers[event.src_path] = JobWatcher(event.src_path)
            self.job_watchers[event.src_path].start()

    def on_deleted(self, event):
        '''Override fSH method. When a dir is removed, job is done. Stop the
        watcher and tell it to clean up'''
        if event.is_directory and event.src_path in self.job_watchers:
            self.job_watchers[event.src_path].shutdown()

    def shutdown(self):
        '''Shutdown the program [Not implemented]'''
        #TODO:  Define shutdown of main watcher
        pass

class JobWatcher(watchdog.events.FileSystemEventHandler, Process):
    '''A class responsible for watching a unique condor submitted job'''

    def __init__(self, watchdir):
        super(JobWatcher, self).__init__()
        self.stat_monitors = set()
        self.__exit = Event()
        self.__watchdir = watchdir

    def on_created(self, event):
        '''Override FSH method to add non-dirs to our list of files to monitor
        size'''
        if not event.is_directory:
            self.stat_monitors.add(event.src_path)

    def on_deleted(self, event):
        '''Override FSH method to remove non-dirs to our list of files to
        monitor size of'''
        if not event.is_directory and event.src_path in self.stat_monitors:
            self.stat_monitors.remove(event.src_path)

    def shutdown(self):
        '''Signal an individual job watcher to shut down'''
        self.__exit.set()

    def run(self):
        '''Main running function for a process watching a particular condor
        job. Creates its own logfile, watches for changes and then exits'''
        observer = Observer()
        observer.schedule(self, self.__watchdir, recursive=True)
        files = 0
        file_space = 0
        job_ad = classad.parseOne(open(self.__watchdir+"/.job.ad", "r"))
        jobdate = datetime.datetime.fromtimestamp(
            int(job_ad['JobStartDate'])).strftime('%Y-%m-%d %H:%M:%S')
        try:
            logname = ''.join([LOG_DIR, job_ad['Owner'], ".",
                               job_ad['UidDomain'], ".", str(job_ad['QDate']),
                               ".", str(job_ad['ClusterId']),
                               ".", str(job_ad['ProcId']), ".log"])
            logfile = open(logname, "wb")
        except IOError:
            sys.stderr.write("Problem creating logfile {0}".format(logname))
            return

        logwriter = csv.writer(logfile)
        logwriter.writerow([job_ad['User'], jobdate])
        observer.start()
        while not self.__exit.is_set():
            time.sleep(1)
            for item in self.stat_monitors.copy():
                try:
                    file_space += os.path.getsize(item)
                except OSError:
                    pass      # File has been deleted during our loop
                files += 1
            logwriter.writerow([int(time.time()), files, file_space])
            files = 0
            file_space = 0
        logfile.close()

        # NOTE: There is a bug (?) in watchdog where if the dir being watched
        # is deleted it cannot be properly closed. It appears to garbage
        # collect and exit just fine without calling stop and join,
        # but this isn't a great solution.
#        observer.stop()
#        observer.join()

    def writelog(self):
        '''Write the desired output to the specified format'''
        #TODO: implement this to extrapolate some of the logic out of the big
        # run function.

def main(args):
    monitor_path = args[1] if len(args) > 1 else CONDOR_WATCHDIR
    CondorWatcher(monitor_path)

if __name__ == "__main__":
    main(sys.argv)
