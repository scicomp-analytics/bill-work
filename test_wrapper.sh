#!/bin/bash
source /etc/profile
echo $(pwd)    # debugging purposes -- can delete
if cat $_CONDOR_JOB_AD | grep 'pkgTool'
then
	cloneProcessor "$@"
	exit $?
fi
exec "$@"
echo "exec failure: $@" > $_CONDOR_WRAPPER_ERROR_FILE 
exit 1
