This repo houses scripts I've been using over the summer. All of the documentation is in flux, and I will reorganize the repo more logically when our servers are back up. This is a first draft and will be heavily revised when I implement everything again from scratch for testing.

The latest version of parrot (6 branch) is assumed for parrot-related scripts.

## Script descriptions: 

### Packing script (parrot_scripts/jobpackage.py)

This script is for capturing jobs submitted through condor via parrot, either for preservation of the experiment or to create a package where another machine can (in theory) use that package instead of having to install the entire software stack. 

Currently I've set the script to use two classad flags in submitted condor jobs:
+pkgTool and +pkgID. +pkgTool has two options: preserve (+pkgTool=Preserve)
and build (+pkgTool=Build). Preserve intercepts a job and completely packages 
it up with parrot, uploading the tarball to s3 [*TODO: change when s3 implementation in script is finalized*]. This mode ignores +pkgID as 
it is note relevant.

+pkgTool=Build signals the worker node to build a parrot package based on
the submitted job. On the user's end this is transparent, and they should receive
their output files from condor as usual. On the worker node side, the +pkgID
flag is scanned to determine where to build the package (and if it already 
exists.) E.g. running +pkgID="IceCube" will signal the worker node to build
a package called IceCube. It does this by scanning the BUILD\_PKG\_DIR directory
for the name of the package--if it cannot find one, it creates a new directory
for the package. Then it carries out parrot\_package\_run and parrot\_package\_create
to build the package (it does not tar it up however). Note that a package
can be added to many times--e.g. you can submit five different scripts with 
+pkgID="IceCube" and parrot will add in any dependencies not currently in the
directory. 

Things which could improved:

*TODO: Improvements and problems. Logging*

### Rerunning script (parrot_scripts/jobrun.py)

This is somewhat simpler. It assumes packages are stored in PKG\_HOST\_DIR
unpacked, and like the packing script takes the name of the subdirectory
to be the name of the package. Currently this relies on the
+parrotRun="package name" flag to be set in a submitted job, which is the
biggest issue. 

#### Example submit files
[create some]

### Parrot\_package\_run 

* Note: currently not uploaded, will upload when servers are back up


Parrot\_package\_run is a bash script for streamlining the process of running
parrot "packages" created by parrot\_package\_create and comes stock with cctools. I had to make a few  modifications to this, notably mounting the execution directory of the condor job running the package, as well 

*TODO: mountlist comments*

### Job spying script (jobwatcher.py)
(Copied from the header in jobwatcher.py)

Script which uses the python watchdog module to monitor number of files and
total directory size for condor jobs. It logs the number of files and size of
files in a job directory over time.

This implementation isn't optimal by any means, but gets the job done. A
master condorwatcher watches the condor directory for new directories (incoming
jobs), and spawns a new process to monitor each job (jobwatcher)

## USAGE (parrot package scripts)
### Packing script: 
1) Modify condor's user job wrapper to call the packing script of +pkgTool 
is set in the classad [see *TODO: EXAMPLE WRAPPER NAME*]
2) Modify BUILD\_PKG\_DIR to whatever directory you would like to use 
to house packages. Set PARROT\_USER to the user your worker node executes jobs
as (NOTE: as of right now this only supports running everything as the same
user). ERROR\_LOG can be set to any file the user running jobs should have access
to. Finally, HOME\_DIR can be deleted assuming your site redirects HOME to
condor's working directory. 

PARROT\_USER is also of note, as currently it is "nobody" because that was
the user *where the packages were created*.

HOME\_DIR is needed mainly for our implementation, which seems to be one
of the only sites not to just point HOME to the \_CONDOR\_SCRATCH\_DIR. 

### Running script: 
1) Modify condor's user job wrapper to call the packing script of +parrotRun 
is set in the classad [see *TODO: EXAMPLE WRAPPER NAME*]
2) Set PKG\_HOST\_DIR to be wherever your parrot packages are stored. Modify
PARROT\_USER to be the user the packages are run as [*TODO: explanation of user issues*], ideally the same user as the user who built the packages with the same GID/SID.

### Job spying script

(Mostly copied from header in jobwatcher.py)
This script isn't daemonized by default but is designed to be run in the 
background. It can be configured with upstart or any service manager of choice.
It is recommended to use taskset to execute to limit the script to two CPUs
(e.g. in case one job creates a massive amount of files and blocks a thread).


It also will not catch jobs which are very short (~1-2 seconds) due to sleep.

Requires classad bindings for python and watchdog package
(http://pythonhosted.org/watchdog/installation.html, sometimes available as
rpm/deb)

CONDOR\_WATCHDIR may need to be changed if different than
/var/lib/condor/execute

LOG\_DIR can be set to designate the desired output log directory. 

For each job, output is a CSV of format:
header row: User@domain, Date
all following rows: Epoch time, Number files, Directory size (bytes)
Each file is saved as user.domain.date.jobID.procID.log
